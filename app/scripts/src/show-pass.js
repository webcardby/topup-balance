$(document).ready(function() {

    $(".group-password").keyup(function () {
       if ($(this).find('input').val()) {
          $(this).find(".show-hide-pass").show();
       }
       else {
          $(this).find(".show-hide-pass").hide();
       }     
    });
    
    $('.group-password').find('input').each(function(index, input) {
        var $input = $(input);
        $input.parent().find('.show-hide-pass').on('click',function(event) {
            event.preventDefault();
            if ($(this).siblings('input').attr("type") == "text"){
               $(this).siblings('input').attr('type', 'password');
                $(this).find('img').addClass( "hide-pass-eye" );
            } else if($(this).siblings('input').attr("type") == "password"){
                $(this).siblings('input').attr('type', 'text');
                $(this).find('img').removeClass( "hide-pass-eye" );
            }
        })
    })
});