$('.navbar-mobile-toggler').on('click', function () {
    $(this).toggleClass('open');
    $('.mobile-side-navbar').toggleClass('mobile-side-navbar_open');
});

$('.select-selected.collapser').click(function() {
    $(this).next().collapse('toggle');
});

$('.nav-link').click(function() {
    $(this).next('ul').collapse('toggle');
});

$('.products-for .switch-btn_right').click(function(){
    $('#myTab > .nav-item > .active').parent().next('li').find('a').trigger('click');
});

$('.products-for .switch-btn_left').click(function(){
    $('#myTab > .nav-item > .active').parent().prev('li').find('a').trigger('click');
});

$(document).ready(function() {
  
    
    $(".review-card").each(function(){
        
        if ($(this).find('.review-text').prop('scrollHeight') < parseInt($('.review-text').css('max-height'))) {
            $(this).find('.more-small').hide();
        } else {
            $(this).find('.more-small').show();
        }
    });
    
    $(".review-card .less-button").hide();

    $(".review-card .more-button").click(function(){ 

      $(this).parent().addClass("more");

      $(this).hide();

      $(this).siblings(".review-card .less-button").show();

    });


    $(".review-card .less-button").click(function(){

      $(this).parent().removeClass("more");

      $(this).hide();

      $(this).siblings(".review-card .more-button").show();

    });
        
});

$(window).on("resize", function(){
    if (window.matchMedia("(max-width:767.9px)").matches) {
        $('#mobile-reorder__maintenance__img').insertBefore('#mobile-reorder__maintenance__form');
        $('#mobile-reorder__user-navbar__country').appendTo('#mainNav .container');
        
        $('.user-panel-section .user-history .user-operation-card').each(function(){
            if ($('.operation-mobile-status').length) {
               //do something if elem is present
            } else {
                $('<div>', {class: 'operation-mobile-status'}).insertAfter($('.user-panel-section .user-history .user-operation-card').find('.operation-status-img'));
            }
        });
        $('.user-panel-section .user-history .user-operation-card').each(function(){
           $(this).find('.operation-mobile-status').append($(this).find('.operation-name'), $(this).find('.operation-status'));
        });
        
        $('.user-panel-section .user-history .user-operation-card').each(function(){
            if ($('.operation-mobile-value').length) {
               //do something if elem is present
            } else {
                $('<div>', {class: 'operation-mobile-value'}).insertAfter($('.user-panel-section .user-history .user-operation-card').find('.operation-mobile-status'));
            }
        });
        $('.user-panel-section .user-history .user-operation-card').each(function(){
           $(this).find('.operation-mobile-value').append($(this).find('.operation-value'), $(this).find('.operation-date'));
        });
        
        $('.navbar .container').each (function(){
          if ($('.mobile-nav-btns').length) {
            } else {
                $(this).append($('<div>', {class: 'mobile-nav-btns'}))
            }
          $(this).find('.mobile-reorder__btn').appendTo('.mobile-nav-btns');
        });
      
        $('#mobile-reorder__navbar__lang').appendTo('.mobile-side-navbar__top');
      
    }
}).resize();