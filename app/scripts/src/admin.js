
$(window).on("resize", function () {
    if (window.matchMedia("(min-width: 1440px)").matches) {
        $('.admin-container').each(function(){
            var acontLeft = $('.admin-container').position().left - $('.navbar-admin-side').outerWidth();
            $('.admin-container').css('margin-left',acontLeft+'px');
        })
    }
}).resize();

$('.admin-setting-item__status_toggle').each(function () {    
    var checked = $(this).find('input').is(':checked');
    $(this).find('.status').text(checked ? 'On' : 'Off');
});

$('.admin-setting-item__status_toggle').each(function () {
    $(this).find('.switch').click(function () {
        var checked = $(this).find('input').is(':checked');
        $(this).siblings('.status').text(checked ? 'On' : 'Off');
    });
});

$('.admin-setting-item__status_toggle').each(function () {
    $(this).find('.switch').click(function () {
        var checked = $(this).find('input').is(':checked');
        $(this).siblings('.status').text(checked ? 'On' : 'Off');
    });
});

$('.admin-staff-item').each(function () {
    $(this).find('.admin-staff-item__switch input').each(function(){
        if($(this).is(":checked")) {
            $(this).parents('.admin-staff-item').addClass('active');
        } else {
            $(this).parents('.admin-staff-item').removeClass('active');
        }
    });
});

$('.admin-staff-item').each(function () {
    $(this).find('.admin-staff-item__switch input').change(function(){
        if($(this).is(":checked")) {
            $(this).parents('.admin-staff-item').addClass('active');
        } else {
            $(this).parents('.admin-staff-item').removeClass('active');
        }
    });
});

$('.admin-edit-card').each(function() {
    $('.admin-blog-item__btns_edit').click(function(event){
        event.preventDefault();
        $(this).parents('.admin-edit-card').addClass('admin-edit-card__editable');
    });
    
    $('.user-account-admin-right__btn .btn').click(function(event){
        event.preventDefault();
        $(this).parents('.admin-edit-card').removeClass('admin-edit-card__editable');
    });
})
