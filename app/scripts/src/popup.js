$(document).ready(function($) {
    $('#popup-contact, #popup-contact-m').on('click', function(event) {
        event.preventDefault();
        $('#popup-contact-form').addClass('is-visible');
    });
    
    $('#popup-ideas, #popup-ideas-m').on('click', function(event) {
        event.preventDefault();
        $('#popup-ideas-form').addClass('is-visible');
    });
    
    $('#popup-partnership, #popup-partnership-m').on('click', function(event) {
        event.preventDefault();
        $('#popup-partnership-form').addClass('is-visible');
    });
    
    $('#terms-and-conditions, #terms-and-conditions-m').on('click', function(event) {
        event.preventDefault();
        $('#terms-and-conditions-form').addClass('is-visible');
    });
    
    $('#privacy-policy, #privacy-policy-m').on('click', function(event) {
        event.preventDefault();
        $('#privacy-policy-form').addClass('is-visible');
    });
    
    $('#faq, #faq_footer, #faq-m').on('click', function(event) {
        event.preventDefault();
        $('#faq-form').addClass('is-visible');
    });
    
    $('.didnt-find-btn').on('click', function(event) {
        event.preventDefault();
        $('#didnt-find-form').addClass('is-visible');
    });

    $('.cd-popup').on('click', function(event) {
        if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    
    $('.cd-popup.semi-automatic').each(function() {
        
        $(this).find('.btn').on('click', function(event) {
            event.preventDefault();
            $(this).parents('.cd-popup.semi-automatic').removeClass('is-visible');
        })
    });
    
});