$('.btn-share.btn-share-closed').click(function() {
    if ( $(this).hasClass('btn-share-closed') ) {
        $(this).switchClass('btn-share-closed','btn-share-open');
        TweenMax.to(shareSocial1, 0.5, { pointerEvents: 'all',
         y: -96, x: 0, opacity:1,  delay:0.2, ease: Cubic.easeInOut
        });
        TweenMax.to(shareSocial2, 0.5, { pointerEvents: 'all',
          y: -70, x: 70, opacity:1, delay:0.1, ease: Cubic.easeInOut
        });
        TweenMax.to(shareSocial3, 0.5, { pointerEvents: 'all',
          y: 0, x: 96, opacity:1, ease: Cubic.easeInOut
        });
    } else if ( $(this).hasClass('btn-share-open') ) {
        $(this).switchClass('btn-share-open','btn-share-closed');
        TweenMax.to(shareSocial1, 0.5, { pointerEvents: 'none',
          y: 0, x: 0, opacity:0, ease: Cubic.easeInOut
        });
        TweenMax.to(shareSocial2, 0.5, { pointerEvents: 'none',
          y: 0, x: 0, opacity:0, ease: Cubic.easeInOut
        });
        TweenMax.to(shareSocial3, 0.5, { pointerEvents: 'none',
          y: 0, x: 0, opacity:0, ease: Cubic.easeInOut
        });
    }
  
    $('#fade-wrapper').toggleClass('on');
    $('.btn-share-block').toggleClass('on');
});