$(document).ready(function() {
    $('.user-account-image').each(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image-upload-img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".image-upload-input").change(function(){
            readURL(this);
        });

        $(".image-upload").click(function(upload) {
            upload.preventDefault();
            $("input.image-upload-input").click();
            $(this).parent().addClass('covered')
        });
    });
});