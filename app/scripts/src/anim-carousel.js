if ($("#card-slider").length > 0) {
    
    var cards = $('#card-slider .slider-item').toArray();

    startAnim(cards);

    function startAnim(array){
        if(array.length >= 0 ) {

            TweenMax.fromTo(array[0], 1, {x:0, y: 0, opacity: 0.33, }, {x:0, y: -50, opacity:0, scale:0.33, zIndex: 0, ease: Cubic.easeInOut, onComplete: sortArray(array)});

            TweenMax.fromTo(array[1], 1, {x:0, y: 80, opacity:0.7,  zIndex: 1},  {x:0, y: 0, opacity:0.33, scale:0.55, zIndex: 0, ease: Cubic.easeInOut});

            TweenMax.fromTo(array[2], 1, {x:0, y: 180, opacity:1,  zIndex: 1}, {x:0, y: 80, opacity:0.70, scale:0.7, boxShadow: '0 0px 0px 0 rgba(136, 145, 155, 0.0)', zIndex: 0, ease: Cubic.easeInOut});

            TweenMax.fromTo(array[3], 1, {x:0, y:280, opacity:0.70}, {x:0,y:180, boxShadow: '0 4px 12px 0 rgba(136, 145, 155, 0.23)', scale:1, zIndex: 1, opacity: 1, ease: Cubic.easeInOut});

            TweenMax.fromTo(array[4], 1, {x:0, y: 360, opacity:0.33, zIndex: 1}, {x:0, y: 280, opacity:0.70, boxShadow: '0 0px 0px 0 rgba(136, 145, 155, 0.0)', zIndex:0, scale:0.7,  ease: Cubic.easeInOut});

            TweenMax.fromTo(array[5], 1, {x:0, y:440, opacity:0, zIndex:0}, {x:0, y:360, opacity: 0.33, scale:0.55, zIndex:0, ease: Cubic.easeInOut});

        } else {
            $('#card-slider').append('<p>Sorry, carousel should contain more than 3 slides</p>')
        }
    };

    function sortArray(array) {
        clearTimeout(delay);
        var delay = setTimeout(function(){
            var firstElem = array.shift();
            array.push(firstElem);
            return startAnim(array); 
        }, 8000)
    }
    
};
